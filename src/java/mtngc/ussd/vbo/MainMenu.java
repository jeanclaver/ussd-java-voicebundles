/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.vbo;

import accessoracledb.CVMBundle;
import accessoracledb.DBSrverClient;
import accessoracledb.VoiceBundle;
import java.util.*;
//import dbaccess.dbsrver.datareseller.*;

import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import ucipclient.UCIPClientEngine;
import ucipclient.UCIPGetAccountDetailsResponse;
//import mtngc.ussd.AbstractMainMenu;
//import mtngc.bundles.core.*;

/**
 *
 * @author JcMoutoh
 */
public final class MainMenu {

    String header;
    String msg;
    String additionalMsg;
    boolean allowed;
    String FreeFlow;
    String msisdn;
    private static int[] supportedServiceClasses = {1, 2, 3, 4, 6, 15, 30, 20, 28, 19, 102, 31, 40, 66, 103, 45};

    public MainMenu(String msisdnStr, String transactionId, boolean confirm) {
        Init(msisdnStr, transactionId, confirm);
    }

    MainMenu(VoiceBundle msisdnStr, String TransactionId, boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    protected void Init(String msisdnStr, String transactionId, boolean confirm) {
        header = "MTN Wakhati";//VOICE OFFER

        try {
            if (!confirm) {

                msisdn = msisdnStr;
                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Fetching Subscriber Service Class details");
                UCIPClientEngine ucipEngine = new UCIPClientEngine();
                UCIPGetAccountDetailsResponse getAccountResponse = ucipEngine.GetAccountDetails(msisdn, transactionId);
                int sc = getAccountResponse.getServiceClass();
                MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|Service Class=" + sc);

//   
                if (!this.IsServiceClassAllowed(sc)) {
                    MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdnStr + "| is not configured to use this service ");
                    MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|Service Class=" + sc+" is not allowed ");
                    msg = NotAllowed();
                    this.FreeFlow = "FC";

                } else {
                    if (this.IsServiceClassAllowed(sc)) {
                        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdnStr + " is configured to use MTN Wakhati Offer ");
                         MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|Service Class=" + sc+" is allowed");
                         
                    }
                    allowed = true;
                     MLogger.Log(this, LogLevel.ALL, "Dsplay the menu for Subscriber:" + msisdn + "|Session:" + transactionId + "|Service Class=" + sc);
                    msg = Process();
                    this.FreeFlow = "FC";
                }
            } else {
                msg = ProcessConfirm();
            }
        } catch (Exception e) {
            MLogger.Log(this, e);
        }
    }

    private boolean IsPos(String msisdn) {
        boolean resp = false;
        DBSrverClient dbClient = new DBSrverClient();
        CVMBundle dbBs = dbClient.getEligibleCVM(msisdn);
        if (dbBs != null) {
            resp = true;
        }
        return resp;
    }

    public String NotAllowed() {

        StringBuilder sb = new StringBuilder();
        sb.append(header).append("\n");
        sb.append("Cher client, vous n etes pas eligible pour cette promo. Tapez *440*3*1# et gagnez jusqu a 50% de Bonnus sur MTN MoMo");
        if (additionalMsg != null) {
            sb.append(additionalMsg);
        }
        this.FreeFlow = "FC";
        String str = sb.toString();
        return str;
    }

    public String getString() {
        return msg;
    }

    public String Process() {
        StringBuilder sb = new StringBuilder();
        sb.append(header + "\n");
        sb.append("\n");
        DBSrverClient dbClient = new DBSrverClient();
        List<VoiceBundle> list = dbClient.getVoicePackage();

        int i = 1;
        for (VoiceBundle vb : list) {
            sb.append(i + ". " + vb.getVoice_bundle_name() + "\n");

            i++;
        }

        sb.append("\n");
        sb.append("Repondez");
        return sb.toString();

    }

    public String ProcessConfirm() {
        StringBuilder sb = new StringBuilder();
        //sb.append(header+"\n");
        sb.append("Veuillez confirmer le numero du client\n");
        sb.append("\n");
        sb.append("Repondez");
        return sb.toString();

    }

    private boolean IsServiceClassAllowed(int sc) {
        for (int n : supportedServiceClasses) {
            if (n == sc) {
                return true;
            }
        }

        return false;
    }

    public boolean isAllowed() {
        return this.allowed;
    }

}

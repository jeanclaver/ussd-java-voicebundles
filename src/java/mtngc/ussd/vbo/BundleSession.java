/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.vbo;
import mtngc.ussd.AbstractBundleSession;
import accessoracledb.VoiceBundle;
/**
 *
 * @author JcMoutoh
 */
public class BundleSession  extends AbstractBundleSession {
    private String ussdSessionId;
    private String msisdn;
    private VoiceBundle selectedPackage;
    private String ussdCode;
    private String freeFlow;
    private int step;
    private int option;
    
    /**
     * @return the ussdSessionId
     */
    @Override
    public String getUssdSessionId() {
        return ussdSessionId;
    }

    /**
     * @param ussdSessionId the ussdSessionId to set
     */
    @Override
    public void setUssdSessionId(String ussdSessionId) {
        this.ussdSessionId = ussdSessionId;
    }

    /**
     * @return the msisdn
     */
    @Override
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn the msisdn to set
     */
    @Override
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * @return the selectedPackage
     */
    public VoiceBundle getSelectedPackage() {
        return selectedPackage;
    }

    /**
     * @param selectedPackage the selectedPackage to set
     */
    public void setSelectedPackage(VoiceBundle selectedPackage) {
        this.selectedPackage = selectedPackage;
    }

    /**
     * @return the ussdCode
     */
    public String getUssdCode() {
        return ussdCode;
    }

    /**
     * @param ussdCode the ussdCode to set
     */
    public void setUssdCode(String ussdCode) {
        this.ussdCode = ussdCode;
    }

    /**
     * @return the freeFlow
     */
    public String getFreeFlow() {
        return freeFlow;
    }

    /**
     * @param freeFlow the freeFlow to set
     */
    public void setFreeFlow(String freeFlow) {
        this.freeFlow = freeFlow;
    }

    /**
     * @return the step
     */
    public int getStep() {
        return step;
    }

    /**
     * @param step the step to set
     */
    public void setStep(int step) {
        this.step = step;
    }

    public int getOption() {
        return option;
    }

    public void setOption(int option) {
        this.option = option;
    }

   
    
}

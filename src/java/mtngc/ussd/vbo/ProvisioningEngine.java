/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.vbo;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import mtngc.sms.SMSClient;
import accessoracledb.CVMBundle;
import accessoracledb.DBSrverClient;
import accessoracledb.VoiceBundle;
import java.util.List;
import ucipclient.UCIPClientEngine;
import ucipclient.UCIPGetAccountDetailsResponse;
import ucipclient.*;

/**
 *
 * @author JcMoutoh
 */
public class ProvisioningEngine {

    public String executeRefillIdRequest(String msisdn, String transactionId, VoiceBundle vBundle, BundleSession bundleSession) {
        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + transactionId + "|Option " + bundleSession.getOption() + " | ==== Initiating Air Provisioning ======.");
        ResponseEnum respEnum = ResponseEnum.SUCCESS;
        StringBuilder sb = new StringBuilder();
        DBSrverClient vbData = new DBSrverClient();
        int option = bundleSession.getOption();
        vBundle = vbData.getVoiceBundle(option);
        VoiceBundle RefillId = vbData.getCurrentBundle(option);
        String refillExternalData = "Wakhati_OFFER";

        UCIPClientEngine ucipEngine = new UCIPClientEngine();
        UCIPGetAccountDetailsResponse getAccountResponse = ucipEngine.GetAccountDetails(msisdn, transactionId);
        MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|" + vBundle.getVoice_bundle_refillId());
        int sc = getAccountResponse.getServiceClass();
        MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|Service Class=" + sc);
        if (getAccountResponse != null) {
            double balance = getAccountResponse.getBalance();
            int price = vBundle.getVoice_bundle_price();
            if (price > balance) {
                respEnum = ResponseEnum.BALANCEINSUFUSANT;
                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + transactionId + "|Option " + bundleSession.getOption() + " | " + respEnum.toString());
            } else {
                MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|Refill ID=" + RefillId);
                UCIPUpdateBalanceAndDateResponse deductMAResponse = ucipEngine.UpdateMainAccountBalanceAndDate(msisdn, transactionId, -price, true, new Date(), refillExternalData);
                int x = deductMAResponse.getResponseCode();
                if ((x == 0) && (!deductMAResponse.isError())) {
                    java.util.Calendar cal = java.util.Calendar.getInstance();
                    cal.add(java.util.Calendar.DATE, 1); // this will add validity days        
                    Date expiry = cal.getTime();
                    if (RefillId != null) {
                        UCIPUpdateRefillProfilIDResponse refillProfileIdResponse = ucipEngine.RefillProfileID(msisdn, transactionId, vBundle.getVoice_bundle_refillId(), refillExternalData);
                        int y = refillProfileIdResponse.getResponseCode();
                        if ((y == 0) && (!refillProfileIdResponse.isError())) {
                            MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|externalData Response: " + refillExternalData);
                            MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|Refill Profil ResponseCode: " + refillProfileIdResponse.getResponseCode());
                            respEnum = ResponseEnum.SUCCESS;
                            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + transactionId + "|Option " + bundleSession.getOption() + " | " + respEnum.toString());
                            vbData.insertVOTransactionDetails(transactionId, msisdn, sc, vBundle.getVoice_bundle_name(), price, vBundle.getVoice_bundle_refillId(), vBundle.getVoice_bundle_validity(), vBundle.getVoice_bundle_benefit(), respEnum.toString());
//                           vbData.insertCvmTransactionDetails(transactionId, msisdn, sc, vBundle.getVoice_bundle_name(), vBundle.getVoice_bundle_price(), Refill_Id, vBundle.getVoice_bundle_validity(), vBundle.getVoice_bundle_benefit(), respEnum.toString());
                        } else {
                            UCIPUpdateBalanceAndDateResponse refillMAResponse = ucipEngine.UpdateMainAccountBalanceAndDate(msisdn, transactionId, +price, true, new Date(), refillExternalData);
                            if ((x == 0) && (!refillMAResponse.isError())) {
                                respEnum = ResponseEnum.ERROR;
                                vbData.insertVOTransactionDetails(transactionId, msisdn, sc, vBundle.getVoice_bundle_name(), price, vBundle.getVoice_bundle_refillId(), vBundle.getVoice_bundle_validity(), vBundle.getVoice_bundle_benefit(), respEnum.toString());
                                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + transactionId + "|Option " + bundleSession.getOption() + " | " + respEnum.toString());
                            }
                        }
                    } else {
                        respEnum = ResponseEnum.ERROR;
                        MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Refill ID:" + RefillId);
                        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + transactionId + "|Option " + bundleSession.getOption() + " | " + respEnum.toString());
                    }
                } else {
                    respEnum = ResponseEnum.ERROR;
                    MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|error:" + deductMAResponse.getMessage());
                    MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId);
                    MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + transactionId + "|Option " + bundleSession.getOption() + " | " + respEnum.toString());
                }
            }
        } else {
            respEnum = ResponseEnum.ERROR;
            MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|error:" + getAccountResponse);
            MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId);
            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + transactionId + "|Option " + bundleSession.getOption() + " | " + respEnum.toString());
        }

        return sb.toString();
    }

    private void SmsNotifcation(String msisdn) {
        msisdn = "224" + msisdn;// "224664222412";
        String msg = "Profitez de 100% bonus sur vos forfaits internet via MyMTN app. Cliquez sur https://goo.gl/rr8xyV .";
        SMSClient smsClient = new SMSClient();
        smsClient.sendSMS("MyMTN", msisdn, msg);

    }

    private void SendPOSSuccessSMS(String msisdn, Double commission) {

        int commissionStr = commission.intValue();

        String msg = "Vous avez beneficiez de  " + commissionStr + " FG sur votre compte principal. Tapez *223# pour le solde.";

        String msisdnStr = msisdn;
        //msisdn = "664222545";
        if (!msisdnStr.startsWith("224")) {
            msisdnStr = "224" + msisdnStr;
        }

        SMSClient smsClient = new SMSClient();
        smsClient.sendSMS("Internet", msisdnStr, msg);

    }
}

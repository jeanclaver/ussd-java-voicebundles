/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.vbo;

import accessoracledb.DBSrverClient;
import accessoracledb.VoiceBundle;
import java.util.Enumeration;
import java.util.List;
import mtngc.sms.SMSClient;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;

/**
 *
 * @author JcMoutoh
 */
public class ConfirmMenu {
    private int option;

    public ConfirmMenu(int option){
        this.option=option;
    }
    
    

    public String getString(){
        StringBuilder sb = new StringBuilder(); 
        
        DBSrverClient dataDa = new DBSrverClient();
        //sb.append("Confirmez vous l achat des forfaits suivants:\n");
        List<VoiceBundle> list = dataDa.getVoicePackages(option);
        int i=1;
        for(VoiceBundle vb : list){
            //Confirmez vous 
            sb.append(""+vb.getVoice_bundle_confirmation_txt()+"\n");
            sb.append(i+ ". Oui\n");
            i++;
        }
        sb.append("# Retour\n");
        sb.append("Repondez");
         
        String str = sb.toString();
                
        return str;
    }

   
    
    

    
}